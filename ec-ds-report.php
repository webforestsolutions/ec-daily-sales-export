<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://webforest.solutions/
 * @since             1.0.0
 * @package           Ec_Ds_Report
 *
 * @wordpress-plugin
 * Plugin Name:       EC Daily Sales Report
 * Plugin URI:        https://adminwebforest@bitbucket.org/webforestsolutions/ec-daily-sales-export.git
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Prinz Nikko
 * Author URI:        http://webforest.solutions/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       ec-ds-report
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-ec-ds-report-activator.php
 */
function activate_ec_ds_report() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-ec-ds-report-activator.php';
	Ec_Ds_Report_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-ec-ds-report-deactivator.php
 */
function deactivate_ec_ds_report() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-ec-ds-report-deactivator.php';
	Ec_Ds_Report_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_ec_ds_report' );
register_deactivation_hook( __FILE__, 'deactivate_ec_ds_report' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-ec-ds-report.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_ec_ds_report() {

	$plugin = new Ec_Ds_Report();
	$plugin->run();

}
run_ec_ds_report();
