<?php

/**
 * Fired during plugin activation
 *
 * @link       http://webforest.solutions/
 * @since      1.0.0
 *
 * @package    Ec_Ds_Report
 * @subpackage Ec_Ds_Report/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Ec_Ds_Report
 * @subpackage Ec_Ds_Report/includes
 * @author     Prinz Nikko <nikko@webforest.solutions>
 */
class Ec_Ds_Report_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
