<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://webforest.solutions/
 * @since      1.0.0
 *
 * @package    Ec_Ds_Report
 * @subpackage Ec_Ds_Report/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Ec_Ds_Report
 * @subpackage Ec_Ds_Report/includes
 * @author     Prinz Nikko <nikko@webforest.solutions>
 */
class Ec_Ds_Report_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
