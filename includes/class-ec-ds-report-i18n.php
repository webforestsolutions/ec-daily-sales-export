<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://webforest.solutions/
 * @since      1.0.0
 *
 * @package    Ec_Ds_Report
 * @subpackage Ec_Ds_Report/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Ec_Ds_Report
 * @subpackage Ec_Ds_Report/includes
 * @author     Prinz Nikko <nikko@webforest.solutions>
 */
class Ec_Ds_Report_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'ec-ds-report',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
